import numpy as np
import matplotlib.pyplot as plt
%matplotlib qt
import pandas as pd
import statsmodels.api as sm

# Importing the dataset
dataset = pd.read_csv('dataset_copy.csv')
X = dataset.iloc[:, 2:33].values
y1 = dataset.iloc[:, 0].values
y2 = dataset.iloc[:, 1].values


"""# Imputer not feasable
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values = 0,strategy = 'mean') 
imputer = imputer.fit(X)
X = imputer.transform(X)
"""

"""from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)"""

from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators = 10, random_state = 0)
#regressor2 = RandomForestRegressor(n_estimators = 10, random_state = 0)
regressor.fit(X, y1)
#regressor2.fit(X, y2)

# Predicting a new result
y_pred = regressor.predict(X)

plt.xlabel('Position level')
plt.ylabel('Salary')
plt.scatter( range(0,len(y_pred)), y_pred, color='olive')
plt.plot( range(0,len(y_pred)), y1, color='blue', linewidth=2)
plt.show()

X1 = np.append(np.ones((len(y_pred),1)).astype(int), X, 1)

"""#backword elemination
X_opt  = X1[:,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]]
regressor_OLS = sm.OLS(y1,X_opt).fit()
regressor_OLS.summary()
#15,18,20
"""

from sklearn.metrics import accuracy_score
accuracy_score(y1,y_pred)






